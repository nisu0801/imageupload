import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor( private http: HttpClient) { }
  uploadImage(fd){
    return this.http.post('http://162.243.171.81:3000/utils/upload/file/imagekit',fd)
    .pipe(map((res: Response) => {
      return res;
    }));
  }
}

import { Component } from '@angular/core';
import { CommonService } from './services/common.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CommonService]
})
export class AppComponent {
  selectedFile: File = null;

  constructor(private commonService: CommonService, private toastrService: ToastrService) { }


  onFileSelected(event) {
    // console.log(event);
    this.selectedFile = <File>event.target.files[0];
    console.log(this.selectedFile);
  }


  onUpload() {
    const fd = new FormData();
    fd.append('file', this.selectedFile);
    this.commonService.uploadImage(fd).subscribe(response => {
      console.log(response)
      this.toastrService.success('Image Uploaded', 'Success!');
    }, error => {
      console.log(error)
      this.toastrService.error('Error occur', 'Error');
    })
  }
}
